# This is a sample Python script.
# for data processing

import warnings

import pandas as pd
import visualization as vis
from modeling import decision_tree, random_forest
from preprocess import preprocess

warnings.filterwarnings("ignore")
# Importing the dataset
customers = pd.read_csv('customers.xls')
newCustomers = pd.read_csv("new_customers.xls")

customers['is_train'] = 1
newCustomers['is_train'] = 0
dataset = pd.concat([customers, newCustomers])

train_set = dataset[dataset['is_train'] == 1]

# Exploratory Data Analysis
vis.segment_with_more_men(train_set)
vis.married_status(train_set)
vis.age_histogram(dataset)
vis.work_experience_histogram(train_set)
vis.age_distribution(train_set)

# Pre-processing
dataset = preprocess(dataset)

# Modeling
decision_tree(dataset[dataset["is_train"] == 1], dataset[dataset['is_train'] == 0])
random_forest(dataset[dataset["is_train"] == 1], dataset[dataset['is_train'] == 0])

