import numpy as np
from matplotlib import pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, confusion_matrix
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.tree import DecisionTreeClassifier

le = LabelEncoder()


def split_data(preprocessed_dataset):
    X_train, X_test = train_test_split(preprocessed_dataset, test_size=0.40, random_state=101,
                                       stratify=preprocessed_dataset['Segmentation'])

    X_train['Segmentation'] = le.fit_transform(X_train['Segmentation'])
    X_test['Segmentation'] = le.fit_transform(X_test['Segmentation'])

    feature_cols = X_train.columns.tolist()
    feature_cols.remove('Segmentation')
    feature_cols.remove('is_train')
    return X_train, X_test, feature_cols


def decision_tree(preprocessed_dataset, test_set):
    X_train, X_test, feature_cols = split_data(preprocessed_dataset)

    classifier = DecisionTreeClassifier(criterion='entropy', random_state=0)
    classifier.fit(X_train[feature_cols], X_train['Segmentation'])

    y_pred = classifier.predict(X_test[feature_cols])
    eval_score = accuracy_score(X_test['Segmentation'], classifier.predict(X_test[feature_cols]))
    print('Decision Tree Evaluation: {}'.format(eval_score))

    cm = confusion_matrix(X_test['Segmentation'], y_pred)
    print(cm)

    y_pred = classifier.predict(test_set[feature_cols])
    pred = le.inverse_transform(y_pred)
    test_set['Segmentation'] = pred

    test_set.to_csv('new_customers_tree.xls', index=False)

    # show_cm(classifier, X_train[feature_cols], X_train["Segmentation"])


def random_forest(preprocessed_dataset, test_set):
    X_train, X_test, feature_cols = split_data(preprocessed_dataset)

    clf = RandomForestClassifier(n_estimators=100)

    clf.fit(X_train[feature_cols], X_train['Segmentation'])

    y_pred = clf.predict(X_test[feature_cols])

    eval_score = accuracy_score(X_test['Segmentation'], clf.predict(X_test[feature_cols]))
    print('Random Forest Evaluation: {}'.format(eval_score))

    cm = confusion_matrix(X_test['Segmentation'], y_pred)
    print(cm)

    y_pred = clf.predict(test_set[feature_cols])
    pred = le.inverse_transform(y_pred)
    test_set['Segmentation'] = pred

    test_set.to_csv('new_customers_random_forest.xls', index=False)

    calculate_importance(clf, feature_cols)


def show_cm(classifier, X_train, y_train):
    from matplotlib.colors import ListedColormap

    X_set, y_set = X_train, y_train
    X1, X2 = np.meshgrid(np.arange(start=X_set[:, 0].min() - 1, stop=X_set[:, 0].max() + 1, step=0.01),
                         np.arange(start=X_set[:, 1].min() - 1, stop=X_set[:, 1].max() + 1, step=0.01))
    plt.contourf(X1, X2, classifier.predict(np.array([X1.ravel(), X2.ravel()]).T).reshape(X1.shape),
                 alpha=0.75, cmap=ListedColormap(('red', 'green')))
    plt.xlim(X1.min(), X1.max())
    plt.ylim(X2.min(), X2.max())
    for i, j in enumerate(np.unique(y_set)):
        plt.scatter(X_set[y_set == j, 0], X_set[y_set == j, 1],
                    c=ListedColormap(('red', 'green'))(i), label=j)
    plt.title('Decision Tree Classification (Training set)')
    plt.xlabel('Age')
    plt.ylabel('Estimated Salary')
    plt.legend()
    plt.show()


def calculate_importance(classifer, feature_names):
    importances = classifer.feature_importances_
    std = np.std([tree.feature_importances_ for tree in classifer.estimators_], axis=0)

    import pandas as pd

    forest_importances = pd.Series(importances, index=feature_names)

    fig, ax = plt.subplots()
    forest_importances.plot.bar(yerr=std, ax=ax)
    ax.set_title("Feature importances using MDI")
    ax.set_ylabel("Mean decrease in impurity")
    fig.tight_layout()

    plt.show()
