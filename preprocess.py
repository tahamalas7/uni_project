import pandas as pd
from sklearn import preprocessing


def preprocess(dataset):
    dataset = pd.get_dummies(dataset, prefix='Gender', columns=['Gender'], drop_first=True)
    dataset = pd.get_dummies(dataset, prefix='Married', columns=['Ever_Married'], drop_first=True)
    dataset = pd.get_dummies(dataset, prefix='Graduated', columns=['Graduated'], drop_first=True)

    dataset['Profession'].fillna('Unknown', inplace=True)
    dataset['Profession'] = dataset['Profession'].astype('str')

    le = preprocessing.LabelEncoder()
    dataset['Profession_Encoded'] = le.fit_transform(dataset['Profession'])
    dataset.drop('Profession', axis=1, inplace=True)

    dataset['Work_Experience'].fillna(dataset['Work_Experience'].mean(), inplace=True)
    dataset['Family_Size'].fillna(round(dataset['Family_Size'].mean()), inplace=True)

    dataset.loc[dataset['Spending_Score'] == 'Low', 'Spending_Score'] = 1
    dataset.loc[dataset['Spending_Score'] == 'Average', 'Spending_Score'] = 2
    dataset.loc[dataset['Spending_Score'] == 'High', 'Spending_Score'] = 3
    dataset['Spending_Score'] = dataset['Spending_Score'].astype('int')
    return dataset
