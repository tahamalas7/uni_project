import matplotlib.pyplot as plt
import seaborn as sns


def segment_with_more_men(dataset):
    gender_data = dataset.groupby(['Gender', 'Segmentation'])['Segmentation'].count()
    print(gender_data['Male'])

    sns.countplot(dataset['Gender'], hue=dataset['Segmentation'])
    plt.title('Gender Distribution')
    plt.show()


def married_status(dataset):
    ever_married_data = dataset.groupby(['Ever_Married', 'Segmentation'])['Segmentation'].count()
    print(ever_married_data)

    sns.countplot(dataset['Ever_Married'], hue=dataset['Segmentation'])
    plt.title('Married Statistics')
    plt.show()


def age_histogram(dataset):
    sns.distplot(dataset['Age'], kde=False)
    plt.title('Age Histogram')
    plt.show()


def work_experience_histogram(dataset):
    sns.distplot(dataset['Work_Experience'], kde=False)
    plt.title('Work Experience Histogram')
    plt.show()


def age_distribution(dataset):
    sns.set_style('whitegrid')
    sns.distplot(dataset[dataset['Segmentation'] == 'A']['Age'], bins=10, color='red')
    sns.distplot(dataset[dataset['Segmentation'] == 'B']['Age'], bins=10, color='blue')
    sns.distplot(dataset[dataset['Segmentation'] == 'C']['Age'], bins=10, color='black')
    sns.distplot(dataset[dataset['Segmentation'] == 'D']['Age'], bins=10, color='green')
    plt.legend(labels=['Seg=A', 'Seg=B', 'Seg=C', 'Seg=D'])
    plt.title('Age Distribution')
    plt.show()
